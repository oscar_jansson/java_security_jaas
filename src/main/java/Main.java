import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;


public class Main {


    public static void main(String[] args) throws NoSuchAlgorithmException, KeyStoreException {


        System.setProperty("java.security.auth.login.config","test.config");
        LoginContext logincontext = null;
        try {
            logincontext = new LoginContext("test", new MyCallbackHandler());
        } catch (LoginException e) {
            System.out.println(e.getMessage());
            System.exit(0);
        }
while(true){
    try {
        logincontext.login();
        System.exit(0);
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
}




    }
}
